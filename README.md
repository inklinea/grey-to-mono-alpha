# Grey To Mono Alpha

Grey To MonoAlpha - An Inkscape Extension

Inkscape 1.1 +

▶ Appears under Extensions>Colour

▶ Converts a Greyscale Image to
  Monochrome with variable Opacity.

▶ Threshold setting to avoid solid colour

▶ Can be applied to stroke / fill or both
